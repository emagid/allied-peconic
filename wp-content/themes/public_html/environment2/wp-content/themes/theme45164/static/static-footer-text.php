<?php /* Static Name: Footer text */ ?>
<div id="footer-text" class="footer-text">
	<?php $myfooter_text = of_get_option('footer_text'); ?>
	
	<?php if($myfooter_text){?>
		<?php echo of_get_option('footer_text'); ?>
	<?php } else { ?>		
		<a href="<?php echo home_url(); ?>/"><img src="http://environment1.acaratesting.com/wp-content/uploads/2013/07/favicon-1.ico" alt="<?php bloginfo('name'); ?>"></a>
		<?php echo date("Y"); ?> &#169; <a href="<?php echo home_url(); ?>/privacy-policy/" title="<?php _e('Privacy Policy', CURRENT_THEME); ?>"><?php _e('Privacy Policy', CURRENT_THEME); ?></a>
	<?php } ?>
	<?php if( is_front_page() ) { ?>
		<!-- {%FOOTER_LINK} -->
	<?php } ?>
</div>