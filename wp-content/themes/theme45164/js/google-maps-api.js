jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
        ['Riverhead, NY', 40.9250843,-72.7041473],
        ['Southhold, NY', 41.0696275,-72.434377]
    ];
               
     var styles = [
                {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      { "saturation": -17 },
      { "weight": 2.5 },
      { "hue": "#ffff00" }
    ]
  },{
    "featureType": "water",
    "stylers": [
      { "hue": "#8800ff" },
      { "lightness": 45 }
    ]
  }
];

map.setOptions({styles: styles});         
    // Info Window Content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h3>Peconic Pediatrics - Riverhead Office</h3>' +
        '<p> 34 Commerce Drive, Suite 2, Riverhead, NY 11901</p>' +        '</div>'],
        ['<div class="info_content">' +
        '<h3>Peconic Pediatrics - Southold Office</h3>' +
        '<p>44210 Middle Road, Route 48, Southold, NY</p>' +
        '</div>']
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(10);
        google.maps.event.removeListener(boundsListener);
    });
    
}