<?php /* Wrapper Name: Header */ ?>
<div class="row">
    <div class="span4" data-motopress-type="static" data-motopress-static-file="static/static-logo.php">
    	<?php get_template_part("static/static-logo"); ?>

    </div>

	<div class="span8">


	
			<div class="socialicons" >
			
<span style="float:right; padding-right:10px;"><a href="https://www.facebook.com/pages/Peconic-Pediatric-Medicine-A-Division-of-Allied-Pediatrics-of-NY/136286069724373?ref=search"><img src="http://peconicpediatrics.com/wp-content/themes/theme45164/images/icons/facebook.png" alt=""></a></span>
				
				<span style=" padding-right:30px;float:right;"><div class="phoneheader" style="font:helvetica; padding-top:9px; font-size:21px;">(631) 722-8880</div></span>

				<span style="float: right;margin-right: 35px;padding-top: 7px;"><?php echo do_shortcode('[google-translator]'); ?></span>
			</div>
	</div>	

<div class="phoneheadermobile"><a href="http://peconicpediatrics.com/thank-you/">(631) 722-8880</a></div>

	

		<div data-motopress-type="static" data-motopress-static-file="static/static-nav.php">
			<?php get_template_part("static/static-nav"); ?>
		</div>		
		<div class="hidden-phone" data-motopress-type="static" data-motopress-static-file="static/static-search.php">
			<?php get_template_part("static/static-search"); ?>
		</div>	

	
		
</div>