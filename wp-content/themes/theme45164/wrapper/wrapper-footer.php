<?php /* Wrapper Name: Footer */ ?>

<div class="row copyright">
	
	<div class="span12">
		<div data-motopress-type="static" data-motopress-static-file="static/static-footer-nav.php">
			<?php get_template_part("static/static-footer-nav"); ?>
		</div>		
	</div>


	<div class="span4 right" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar" style="float:right;">
        <?php dynamic_sidebar("footer-sidebar"); ?>
    </div>

    <div class="span4">		
		<div data-motopress-type="static" data-motopress-static-file="static/static-footer-text.php">	
			<?php get_template_part("static/static-footer-text"); ?>		
		</div>
	</div>
<div class="span4 middlefooter" style="">
<a href="http://alliedphysiciansgroup.com/" target="_blank"><img src="http://peconicpediatrics.com/wp-content/uploads/2015/02/Peconic-Pediatric-with-Allied-Physicians-Group.png" alt="Peconic Pediatrics is a member of the Allied Physicians Group" align="center"></a>
</div>	
</div>