<?php
/**
 * Service Box
 *
 */
if (!function_exists('service_box_shortcode')) {

	function service_box_shortcode($atts, $content = null) { 
	    extract(shortcode_atts(
	        array(
				'title' => '',
				'subtitle' => '',
				'icon' => '',
				'text' => '',
				'btn_text' => __("More info", CURRENT_THEME),
				'btn_link' => '',
				'btn_size' => '',
				'target' => '',
				'custom_class' => ''
	    ), $atts));
		
		$template_url = get_stylesheet_directory_uri();
	 
		$output =  '<div class="service-box '.$custom_class.'">';
		
		// check what icon user selected
		switch ($icon) {
			case 'no':
				break;
	       	case 'icon1':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
	       	case 'icon2':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
			case 'icon3':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;			
	    }

	   $output .= '<div class="service-box_body">';
	 
		if ($title!="") {
			$output .= '<h3 class="title">';
			$output .= $title;
			$output .= '</h3>';
		}	
		$output .= '<div class="clear"></div>';
		if ($subtitle!="") {
			$output .= '<h6 class="sub-title">';
			$output .= $subtitle;
			$output .= '</h6>';
		}		
		if ($text!="") {
			$output .= '<div class="service-box_txt">';
			$output .= $text;
			$output .= '</div>';
		}		
		if ($btn_link!="") {	
			$output .=  '<div class="btn-align"><a href="'.$btn_link.'" title="'.$btn_text.'" class="btn btn-'.$btn_size.' btn-primary " target="'.$target.'">';
			$output .= $btn_text;
			$output .= '</a></div>';
		}
		$output .= '</div>';	 
		$output .= '</div><!-- /Service Box -->';	 
	    return $output;	 
	} 
	add_shortcode('service_box', 'service_box_shortcode');

}?>